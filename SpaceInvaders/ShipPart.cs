﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceInvaders
{
    class ShipPart
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int ShipWidth { get; set; }
        public int ShipHeight { get; set; }

        public ShipPart()
        {
        }

        public ShipPart(int _x, int _y)
        {
            X = _x;
            Y = _y;
            ShipWidth = 15;
            ShipHeight = 30;
        }

        public ShipPart(int _x, int _y, int width, int height)
        {
            X = _x;
            Y = _y;
            ShipWidth = width;
            ShipHeight = height;
        }
    }
}
