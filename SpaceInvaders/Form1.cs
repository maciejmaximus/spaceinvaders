﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SpaceInvaders;

namespace SpaceInvaders
{
    public partial class Form1 : Form
    {
        Ship ship = new Ship();
        List<Alien> aliens = new List<Alien>();
        List<Bullet> bullets = new List<Bullet>();
        List<AlienBullet> alienBullets = new List<AlienBullet>();
        bool spaceStillPressed = false;

        public Form1()
        {
            InitializeComponent();

            //zmiana Parent dla lbEndGame by był transparent
            var pos = this.PointToScreen(lbEndGame.Location);
            pos = pbGameField.PointToClient(pos);
            lbEndGame.Parent = pbGameField;
            lbEndGame.Location = pos;

            new Settings();
            StartGame();

            timer1.Interval = 1000 / Settings.Timer1Speed;
            timer1.Tick += DrawShip;
            timer1.Start();

            timer2.Interval = 1000 / Settings.Timer2Speed;
            timer2.Tick += MoveAliens;
            timer2.Start();

            timer3.Interval = 1000 / Settings.Timer3Speed;
            timer3.Tick += GenerateAlienBullet;
            timer3.Start();
        }

        public void DrawShip(object sender, EventArgs e)
        {
            //wyswietlanie punktów
            lbPoints.Text = Convert.ToString(Settings.PointsTotal);
            //wyświetlanie levelu
            lbLevel.Text = Settings.Level.ToString();

            if (!Settings.GameOver)
            {
                int pbFieldWidth = pbGameField.Size.Width;

                if (KeyboardKeys.KeyPressed(Keys.Left))
                {
                    ship.Move(-5, pbFieldWidth);
                }
                if (KeyboardKeys.KeyPressed(Keys.Right))
                {
                    ship.Move(5, pbFieldWidth);
                }

                if (KeyboardKeys.KeyPressed(Keys.Space) && !spaceStillPressed)
                {
                    bullets.Add(new Bullet(ship.Parts[(int)ShipParts.Kadlub].X + 5, ship.Parts[(int)ShipParts.Kadlub].Y));
                }

                if (KeyboardKeys.KeyPressed(Keys.Space))
                {
                    spaceStillPressed = true;
                }
                else
                {
                    spaceStillPressed = false;
                }

                //ruch kul i zderzenie kuli z obcym
                BulletsMove();

                //ruch kul obcych i zderzenie ze statkiem
                AliensBulletsMove();

                //kasowanie kul i obcych z ustawioną flagą DeleteMe
                DeletingElements();
            }
            else
            {
                if (KeyboardKeys.KeyPressed(Keys.Enter))
                {
                    StartGame();
                    new Settings();
                }
            }

            pbGameField.Invalidate();
        }

        private void pbGameField_Paint(object sender, PaintEventArgs e)
        {
            if (!Settings.GameOver)
            {
                Graphics gamefieldPB = e.Graphics;
                Brush shipColor = Brushes.White;
                Brush alienColor = Brushes.GreenYellow;
                Brush bulletColor = Brushes.Orange;
                Brush alienBulletColor = Brushes.Red;

                foreach (ShipPart shipPart in ship.Parts)
                {
                    gamefieldPB.FillEllipse(shipColor, shipPart.X, shipPart.Y, shipPart.ShipWidth, shipPart.ShipHeight);
                }

                foreach (Alien alien in aliens)
                {
                    gamefieldPB.FillEllipse(alienColor, alien.X, alien.Y, alien.Width, alien.Height);
                }

                for (int i = 0; i < bullets.Count; i++)
                {
                    gamefieldPB.FillEllipse(bulletColor, bullets[i].X, bullets[i].Y, bullets[i].Width, bullets[i].Height);
                }

                foreach (AlienBullet alienBullet in alienBullets)
                {
                    gamefieldPB.FillEllipse(bulletColor, alienBullet.X, alienBullet.Y, alienBullet.Width, alienBullet.Height);
                }
            }
            else
            {
                lbEndGame.Text = "Koniec gry\nUzyskany wynik: " + Settings.PointsTotal + "pkt\nWciśnij Enter by ponownie rozpocząć";
                lbEndGame.Visible = true;
            }
        }

        public void StartGame()
        {
            ship.Parts.Clear();
            bullets.Clear();
            aliens.Clear();
            alienBullets.Clear();
            lbEndGame.Visible = false;

            int fieldBottom = pbGameField.Size.Height;
            int shipMainPartXStartPos = pbGameField.Size.Width / 2 - 10;
            int shipMainPartYStartPos = fieldBottom - 40;

            var shipPartsStart = new[] { new ShipPart(shipMainPartXStartPos, shipMainPartYStartPos, 15, 40),
                                         new ShipPart(shipMainPartXStartPos - 10, fieldBottom - 25),
                                         new ShipPart(shipMainPartXStartPos + 10, fieldBottom - 25)};

            ship.Parts.AddRange(shipPartsStart);

            for (int i = 0; i < Settings.AliensAmount; i++)
            {
                if (i <= 10)
                {
                    aliens.Add(new Alien(i * 40 + Settings.Margin, 20, 30, 30));
                }
                else if(i <= 21)
                {
                    aliens.Add(new Alien((i - 11) * 40 + Settings.Margin, 60, 30, 30));
                }
                else if (i <= 32)
                {
                    aliens.Add(new Alien((i - 22) * 40 + Settings.Margin, 100, 30, 30));
                }
                else if (i <= 43)
                {
                    aliens.Add(new Alien((i - 33) * 40 + Settings.Margin, 140, 30, 30));
                }
                else if (i <= 54)
                {
                    aliens.Add(new Alien((i - 44) * 40 + Settings.Margin, 180, 30, 30));
                }
            }
        }

        public void MoveAliens(object sender, EventArgs e)
        {
            int leftBorderPoint = 0;
            int rightBorderPoint = pbGameField.Size.Width - 30;

            if (!Settings.HitEdge)
            {
                foreach (Alien alien in aliens)
                {
                    if (alien.HitShip(ship))
                    {
                        EndGame();
                        break;
                    }
                    alien.MoveASide();
                    
                    if (alien.X >= rightBorderPoint || alien.X <= leftBorderPoint)
                    {
                        Settings.HitEdge = true;
                    }
                    
                }
            }
            else
            {
                foreach (Alien alien in aliens)
                {
                    alien.MoveDown();
                }
                if (Settings.Direction)
                {
                    Settings.Direction = false;
                }
                else
                {
                    Settings.Direction = true;
                }
                Settings.HitEdge = false;
            }
        }

        public void GenerateAlienBullet(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int alienShootingNumber = rnd.Next(0, aliens.Count);
            int bulletXcoord = aliens[alienShootingNumber].X + aliens[alienShootingNumber].Width / 2;
            int bulletYcoord = aliens[alienShootingNumber].Y + aliens[alienShootingNumber].Height / 2;

            alienBullets.Add(new AlienBullet(bulletXcoord, bulletYcoord));

        }

        public void BulletsMove()
        {
            for (int i = 0; i < bullets.Count; i++)
            {
                bullets[i].Move();

                for (int j = 0; j < aliens.Count; j++)
                {
                    if (bullets[i].Hit(aliens[j]))
                    {
                        aliens[j].DeleteMe = true;
                        bullets[i].DeleteMe = true;
                        Settings.PointsTotal += Settings.Points;
                    }
                }
            }
        }

        public void AliensBulletsMove()
        {
            int fieldSize = pbGameField.Size.Height;

            for (int i = 0; i < alienBullets.Count; i++)
            {
                alienBullets[i].Move(fieldSize);
                if (alienBullets[i].HitShip(ship))
                {
                    EndGame();
                }
            }
        }

        public void DeletingElements()
        {
            //kasowanie kul
            for (int i = bullets.Count - 1; i >= 0; i--)
            {
                if (bullets[i].DeleteMe)
                {
                    bullets.RemoveAt(i);
                }
            }
            //kasowanie obcych
            for (int i = aliens.Count - 1; i >= 0; i--)
            {
                if (aliens[i].DeleteMe)
                {
                    aliens.RemoveAt(i);
                }
            }

            //sprawdzanie warunku konczacego gre
            if (aliens.Count == 0)
            {
                //EndGame();
                if (Settings.Level != 9)
                {
                    NextLevel();
                }
                else
                {
                    EndGame();
                }
            }
        }

        public void NextLevel()
        {
            StartGame();
            Settings.Level += 1;
        }

        public void EndGame()
        {
            Settings.GameOver = true;
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            KeyboardKeys.KeyValueChange(e.KeyCode, false);
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            KeyboardKeys.KeyValueChange(e.KeyCode, true);
        }
    }
}
