﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceInvaders
{
    class Bullet
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public bool DeleteMe { get; set; }

        public Bullet(int x, int y)
	    {
            X = x;
            Y = y;
            Height = 15;
            Width = 5;
            DeleteMe = false;
	    }

        public void Move()
        {
            Y -= 8;

            if(this.Y < 0 - this.Height)
            {
                this.DeleteMe = true;
            }
        }

        public bool Hit(Alien alien)
        {
            int bulletElipseCentreX = X + Width/2;
            int bulletElipseCentreY = Y + Height/2;
            int alienElipseCentreX = alien.X + alien.Width/2;
            int alienElipseCentreY = alien.Y + alien.Height/2;
            int distance = DistanceBetween2Points(bulletElipseCentreX, bulletElipseCentreY, alienElipseCentreX, alienElipseCentreY);
            int hitDistance = alien.Height/2 + Height/2 -5;

            if(distance < hitDistance)
            {
                return true;
            }
            return false;
        }

        private int DistanceBetween2Points(int x1, int y1, int x2, int y2)
        {
            return Convert.ToInt32(Math.Sqrt(Math.Pow(Convert.ToDouble(x2) - Convert.ToDouble(x1),2) + Math.Pow(Convert.ToDouble(y2) - Convert.ToDouble(y1),2)));
        }
    }
}
