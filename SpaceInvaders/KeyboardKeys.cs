﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SpaceInvaders
{
    class KeyboardKeys
    {
        private static Dictionary<Keys, bool> keyboardKeysDictionary = new Dictionary<Keys, bool>();

        public static bool KeyPressed(Keys key)
        {
            try
            {
                return (bool)keyboardKeysDictionary[key];
            }
            catch (KeyNotFoundException)
            {
                return false;
            }
        }

        public static void KeyValueChange(Keys key, bool value)
        {
            keyboardKeysDictionary[key] = value;
        }
    }
}
