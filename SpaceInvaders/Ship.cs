﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceInvaders
{
    public enum ShipParts
    {
        Kadlub,
        LeweSkrzydlo,
        PraweSkrzydlo
    }

    class Ship
    {
        public List<ShipPart> Parts { get; set; }

        public Ship()
        {
            Parts = new List<ShipPart>();
        }

        public void Move(int shipMoveStep, int fieldWidth)
        {
            int maxXPosMainPart = fieldWidth - Parts[0].ShipWidth - Parts[2].ShipWidth + 5;
            int minXPosMainPart = 0 + Parts[1].ShipWidth - 5;
            for (int i = 0; i < this.Parts.Count; i++)
            {
                Parts[i].X = Parts[i].X + shipMoveStep;
                if(Parts[(int)ShipParts.Kadlub].X > maxXPosMainPart)
                {
                    Parts[(int)ShipParts.Kadlub].X = maxXPosMainPart;
                    Parts[(int)ShipParts.LeweSkrzydlo].X = maxXPosMainPart - 10;
                    Parts[(int)ShipParts.PraweSkrzydlo].X = maxXPosMainPart + 10;
                    break;
                }
                if (Parts[(int)ShipParts.Kadlub].X < minXPosMainPart)
                {
                    Parts[(int)ShipParts.Kadlub].X = minXPosMainPart;
                    Parts[(int)ShipParts.LeweSkrzydlo].X = minXPosMainPart - 10;
                    Parts[(int)ShipParts.PraweSkrzydlo].X = minXPosMainPart + 10;
                    break;
                }
            }
        }
    }
}
