﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceInvaders
{
    class Alien
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public bool DeleteMe { get; set; }

        public Alien(int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
            DeleteMe = false;
        }

        public void MoveASide()
        {
            if (Settings.Direction)
            {
                X += 10;
            }
            else
            {
                X -= 10;
            }
        }

        public void MoveDown()
        {
            Y += this.Height;
        }

        public bool HitShip(Ship ship)
        {
            int shipCentreX = ship.Parts[(int)ShipParts.Kadlub].X + ship.Parts[(int)ShipParts.Kadlub].ShipWidth / 2;
            int shipCentreY = ship.Parts[(int)ShipParts.Kadlub].Y + ship.Parts[(int)ShipParts.Kadlub].ShipHeight / 2;
            int alienElipseCentreX = this.X + this.Width / 2;
            int alienElipseCentreY = this.Y + this.Height / 2;
            int distance = DistanceBet2Points(alienElipseCentreX, alienElipseCentreY, shipCentreX, shipCentreY);
            int hitDistance = 35 + Width/2 - 5;

            if (distance < hitDistance)
            {
                return true;
            }
            return false;
        }

        private int DistanceBet2Points(int x1, int y1, int x2, int y2)
        {
            return Convert.ToInt32(Math.Sqrt(Math.Pow(Convert.ToDouble(x2) - Convert.ToDouble(x1), 2) + Math.Pow(Convert.ToDouble(y2) - Convert.ToDouble(y1), 2)));
        }
    }
}
