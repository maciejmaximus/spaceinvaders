﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceInvaders
{
    class AlienBullet
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public bool DeleteMe { get; set; }

        public AlienBullet(int x, int y)
	    {
            X = x;
            Y = y;
            Height = 15;
            Width = 5;
            DeleteMe = false;
	    }

        public void Move(int pbFieldSizeY)
        {
            Y += 5;

            if(this.Y > pbFieldSizeY)
            {
                this.DeleteMe = true;
            }
        }

        public bool HitShip(Ship ship)
        {
            int shipCentreX = ship.Parts[(int)ShipParts.Kadlub].X + ship.Parts[(int)ShipParts.Kadlub].ShipWidth / 2;
            int shipCentreY = ship.Parts[(int)ShipParts.Kadlub].Y + ship.Parts[(int)ShipParts.Kadlub].ShipHeight / 2;
            int alienElipseCentreX = this.X + this.Width / 2;
            int alienElipseCentreY = this.Y + this.Height / 2;
            int distance = DistanceBetween2Points(alienElipseCentreX, alienElipseCentreY, shipCentreX, shipCentreY);
            int hitDistance = ship.Parts[(int)ShipParts.Kadlub].ShipHeight + Width / 2 - 25;

            if (distance < hitDistance)
            {
                return true;
            }
            return false;
        }

        private int DistanceBetween2Points(int Ax, int Ay, int Bx, int By)
        {
            return Convert.ToInt32(Math.Sqrt(Math.Pow(Convert.ToDouble(Bx) - Convert.ToDouble(Ax),2) + Math.Pow(Convert.ToDouble(By) - Convert.ToDouble(Ay),2)));
        }
    }
}
