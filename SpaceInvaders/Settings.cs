﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpaceInvaders
{
    class Settings
    {
        public static bool GameOver { get; set; }
        public static int Timer1Speed { get; set; }
        public static int Timer2Speed { get; set; }
        public static int Timer3Speed { get; set; }
        public static int AliensAmount { get; set; }
        public static int PointsTotal { get; set; }
        public static int Points { get; set; }
        public static int Margin { get; set; }
        public static bool Direction { get; set; }
        public static bool HitEdge { get; set; }
        public static int Level { get; set; }

        public Settings()
        {
            GameOver = false;
            Timer1Speed = 1000;
            Timer2Speed = 1;
            Timer3Speed = 2;
            AliensAmount = 55;
            PointsTotal = 0;
            Points = 20;
            Margin = 40;
            Direction = true;
            HitEdge = false;
            Level = 1;
        }

    }
}
